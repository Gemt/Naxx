/*
-- Query: select * from cmangos.dbscripts_on_creature_movement where id = 1606101
LIMIT 0, 10000

-- Date: 2017-07-27 11:30
*/
INSERT INTO `dbscripts_on_creature_movement` (`id`,`delay`,`command`,`datalong`,`datalong2`,`buddy_entry`,`search_radius`,`data_flags`,`dataint`,`dataint2`,`dataint3`,`dataint4`,`x`,`y`,`z`,`o`,`comments`) VALUES 
                                         (1606101,   0,        3,        0,         0,          0,              0,          0,           0,        0,         0,         0,        0,  0,  0,  1.4396, 'Turns toward trainee');
                                         (1606101,   1,        1,        26,        0,          16803,          5,          0,           0,        0,         0,         0,        0,  0,  0,  0,      'change emote state: stand');
                                         (1606101,   2,        3,        0,         0,          16803,          5,          0,           0,        0,         0,         0,        0,  0,  0,  4.4563, 'Turns toward Razuvious');
                                         (1606101,   3,        1,        5,         0,          0,              0,          0,           0,        0,         0,         0,        0,  0,  0,  0,      'Shouts');
                                         (1606101,   5,        1,        1,         0,          16803,          5,          0,           0,        0,         0,         0,        0,  0,  0,  0,      'talks');
                                         (1606101,   8,        1,        66,        0,          16803,          5,          0,           0,        0,         0,         0,        0,  0,  0,  0,      'salutes');
                                         (1606101,   11,       3,        0,         0,          16803,          5,          0,           0,        0,         0,         0,        0,  0,  0,  2.03,   'Turns toward dummy');
                                         (1606101,   11,       1,        333,       0,          16803,          5,          0,           0,        0,         0,         0,        0,  0,  0,  0,      'change emote state: train');
