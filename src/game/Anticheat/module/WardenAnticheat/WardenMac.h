/*
 * Copyright (C) 2010-2013 Anathema Engine project <http://valkyrie-wow.com/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 * Copyright (C) 2016-2017 Elysium Project <https://www.elysium-project.org>
 */

#ifndef _WARDEN_MAC_H
#define _WARDEN_MAC_H

#include "Warden.h"

class WorldSession;
class Warden;

class WardenMac : public Warden
{
public:
    WardenMac();
    ~WardenMac();

    WardenModule* GetModuleForClient();

    void Init(WorldSession* pClient, BigNumber* k);
    void RequestHash();
};

#endif